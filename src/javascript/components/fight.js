import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const leftHealthBar = document.getElementById('left-fighter-indicator');
    const rightHealthBar = document.getElementById('right-fighter-indicator');
    let firstFighterHealthDefault = firstFighter.health;
    let firstFighterHealth = firstFighter.health;
    let secondFighterHealthDefault = secondFighter.health;
    let secondFighterHealth = secondFighter.health;
    document.addEventListener('keydown', (e) => {
      if (`Key${e.key.toUpperCase()}` === controls.PlayerOneAttack) {
        console.log(getDamage(firstFighter, secondFighter));

        secondFighterHealth = secondFighterHealth - getDamage(firstFighter, secondFighter);
        rightHealthBar.style.width = `${(secondFighterHealth / secondFighterHealthDefault) * 100}%`;
      }
      if (`Key${e.key.toUpperCase()}` === controls.PlayerTwoAttack) {
        firstFighterHealth = firstFighterHealth - getDamage(secondFighter, firstFighter);
        leftHealthBar.style.width = `${(firstFighterHealth / firstFighterHealthDefault) * 100}%`;
      }
      console.log('1', firstFighterHealth);
      console.log('2', secondFighterHealth);
      if (firstFighterHealth <= 0) {
        leftHealthBar.style.width = '0';
        resolve(secondFighter);
      } else if (secondFighterHealth <= 0) {
        rightHealthBar.style.width = '0';
        resolve(firstFighter);
      }
    });
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage > 0) {
    return damage;
  } else {
    return 0;
  }
}

export function getHitPower(fighter) {
  const hitChance = Math.random() + 1;
  const powerAttack = fighter.attack * hitChance;
  return powerAttack;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const powerDefense = fighter.defense * dodgeChance;
  return powerDefense;
}
