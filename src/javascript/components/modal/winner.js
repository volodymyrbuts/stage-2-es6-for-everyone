import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  const bodyElement = createFighterImage(fighter);
  const title = `${fighter.name} Hадер дупцю`;
  showModal({ title, bodyElement });
}
