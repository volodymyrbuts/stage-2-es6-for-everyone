import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const { attack = 1, defense = 1, health = 1, name = 'Vova', source = '' } = fighter;
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  fighterElement.innerHTML = `<img src=${source}>
  <div>
  <h2> ${name}</h2>
  <p>attack: ${attack}</p>
  <p>defense: ${defense}</p>
  <p>health: ${health}</p>
  </div>
 `;
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
